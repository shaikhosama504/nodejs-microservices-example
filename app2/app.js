const express = require('express');
const app = express();
const axios = require('axios'); // Import axios library
const PORT = 3000;

app.get('/', (req, res) => {
  res.send('Hello from your app2 microservice!');
});


app.get('/getDataFromApp1', async (req, res) => {
  try {
    // Fetch data from app2 using axios
    const response = await axios.get('http://app1:3000/getData');
    res.send(response.data);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
