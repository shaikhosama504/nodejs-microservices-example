const express = require('express');
const app = express();
const PORT = 3000;

const sampleData = {
  "from": "app1",
}

app.get('/', (req, res) => {
  res.send('Hello from your app1 microservice!');
});

app.post('/getData', (req, res) => {
  res.send(sampleData);
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
