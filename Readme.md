# Clone
`git clone https://gitlab.com/shaikhosama504/nodejs-microservices-example.git`\
`cd nodejs-microservices-example`

# Start containers
`docker-compose up -d`

# Stop containers
`docker-compose stop`

# Clean-up
**Remove containers and network:**\
`docker-compose down`

**Remove images:**\
`docker rmi nodejs-microservices-example_app1 && docker rmi nodejs-microservices-example_app2`